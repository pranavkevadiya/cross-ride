package com.crossover.techtrial.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.crossover.techtrial.dto.TopDriverDTO;
import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.model.Ride;
import com.crossover.techtrial.repositories.RideRepository;

public class RideServiceImplTest {

    private static final long COUNT = 3;
    private static final Long RIDE_ID = 1l;
    private static final Long DISTANCE = 100l;

    private static final LocalDateTime startTime = LocalDateTime.now();
    private static final LocalDateTime endTime = LocalDateTime.now();
    private Ride ride;

    @InjectMocks
    private RideServiceImpl rideServiceImpl;

    @Mock
    private RideRepository rideRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        ride = createRide();
    }

    @Test
    public void testGetTopNDrivers() {
        when(rideRepository.findTopDriversBetweenStartTimeAndEndTime(any(String.class), any(String.class)))
        .thenReturn(getTopDrivers());
        List<TopDriverDTO> driverDTOs = rideServiceImpl.getTopDrivers(COUNT, startTime, endTime);
        // Assert
        verify(rideRepository).findTopDriversBetweenStartTimeAndEndTime(any(String.class), any(String.class));
        assertEquals("driver1", driverDTOs.get(0).getName());
        assertEquals("driver2", driverDTOs.get(1).getName());
        assertEquals("driver3", driverDTOs.get(2).getName());
        assertEquals(new Double(10), driverDTOs.get(0).getAverageDistance());
        assertEquals(new Long(20000), driverDTOs.get(0).getMaxRideDurationInSecods());
        assertEquals(COUNT, driverDTOs.size());
    }

    @Test
    public void testFindById() {
        when(rideRepository.findById(RIDE_ID)).thenReturn(Optional.of(ride));
        Ride returnedValue = rideServiceImpl.findById(RIDE_ID);
        verify(rideRepository, times(1)).findById(RIDE_ID);
        assertNotNull(returnedValue);
        assertEquals(RIDE_ID, returnedValue.getId());
    }

    @Test
    public void testSave() {
        rideServiceImpl.save(ride);
        verify(rideRepository, times(1)).save(ride);
    }

    private  List<TopDriverDTO> getTopDrivers() {
        List<TopDriverDTO> topDriverDTOs = new ArrayList<>();
        TopDriverDTO topDriver1 = new TopDriverDTO();
        topDriver1.setName("driver1");
        topDriver1.setEmail("driver1@gmail.com");
        topDriver1.setAverageDistance(10d);
        topDriver1.setMaxRideDurationInSecods(20000l);
        topDriver1.setTotalRideDurationInSeconds(5000l);

        TopDriverDTO topDriver2 = new TopDriverDTO();
        topDriver2.setName("driver2");
        topDriver2.setEmail("driver2@gmail.com");
        topDriver2.setAverageDistance(15d);
        topDriver2.setMaxRideDurationInSecods(8000l);
        topDriver2.setTotalRideDurationInSeconds(11000l);

        TopDriverDTO topDriver3 = new TopDriverDTO();
        topDriver3.setName("driver3");
        topDriver3.setEmail("driver3@gmail.com");
        topDriver3.setAverageDistance(12d);
        topDriver3.setMaxRideDurationInSecods(200l);
        topDriver3.setTotalRideDurationInSeconds(500l);

        topDriverDTOs.add(topDriver1);
        topDriverDTOs.add(topDriver2);
        topDriverDTOs.add(topDriver3);
        return topDriverDTOs;
    }

    private static Ride createRide() {
        Person rider = new Person();
        rider.setId(1L);
        rider.setEmail("rider1@gmail.com");
        rider.setName("Rider 1");
        rider.setRegistrationNumber("RG100");

        Person driver = new Person();
        driver.setId(2L);
        driver.setEmail("driver1@gmail.com");
        driver.setName("Driver 1");
        driver.setRegistrationNumber("RG101");

        Ride ride = new Ride();
        ride.setId(RIDE_ID);
        ride.setDistance(DISTANCE);
        ride.setStartTime(startTime.toString());
        ride.setEndTime(endTime.toString());
        ride.setDriver(driver);
        ride.setRider(rider);
        return ride;
    }

}
