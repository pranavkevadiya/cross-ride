package com.crossover.techtrial.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.crossover.techtrial.model.Person;
import com.crossover.techtrial.repositories.PersonRepository;

public class PersonServiceImplTest {

    @InjectMocks
    private PersonServiceImpl personServiceImpl;

    @Mock
    private PersonRepository personRepository;

    private Person person;
    
    private static final Long PERSON_ID = 1l;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        person = createPerson();
    }
    
    private Person createPerson() {
        Person person = new Person();
        person.setEmail("person@test.com");
        person.setId(1L);
        person.setName("PersonName");
        person.setRegistrationNumber("REG001");
        return person;
    }
    
    @Test
    public void testFindById() {
        when(personRepository.findById(PERSON_ID)).thenReturn(Optional.of(person));
        Person returnedValue = personServiceImpl.findById(PERSON_ID);
        verify(personRepository, times(1)).findById(PERSON_ID);
        assertNotNull(returnedValue);
        assertEquals(PERSON_ID, returnedValue.getId());
    }

    @Test
    public void testGetAll() {
        when(personRepository.findAll()).thenReturn(Lists.newArrayList(person));
        List<Person> returnedValue = personServiceImpl.getAll();
        verify(personRepository, times(1)).findAll();
        assertNotNull(returnedValue);
        assertEquals(PERSON_ID, returnedValue.get(0).getId());
    }

    @Test
    public void testSave() {
        personServiceImpl.save(person);
        verify(personRepository, times(1)).save(person);
    }


}
